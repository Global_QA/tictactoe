const br = document.createElement('br');

onLoad = () =>{
    let gameInvitationDiv = document.createElement('fieldset');
    gameInvitationDiv.id = 'gameInvitationDiv';
    gameInvitationDiv.textContent = 'Do you want to play "X" or "O"?';
    gameInvitationDiv.appendChild(br);
    gameInvitationDiv.appendChild(br);
    gameInvitationDiv.appendChild(br);
    let xSelector = document.createElement('input');
    xSelector.name = 'xORoSelector';
    xSelector.type = 'radio';
    xSelector.value = 'X';
    xSelector.onchange = startGame;
    let oSelector = document.createElement('input');
    oSelector.name = 'xORoSelector';
    oSelector.type = 'radio';
    oSelector.value = 'O';
    oSelector.onchange = startGame;
    gameInvitationDiv.appendChild(xSelector);
    gameInvitationDiv.appendChild(oSelector);
    gameInvitationDiv.appendChild(br);
    let whoStartsText = document.createElement('p');
    whoStartsText.textContent = 'remember: "X" always starts the game';
    gameInvitationDiv.appendChild(whoStartsText);
    document.body.appendChild(gameInvitationDiv);
};

restartGame = () => {
  document.body.removeChild(gameField);
  document.body.removeChild(restartGameDiv);
  onLoad();
};

startGame = () => {

    let whoIsTheUser = document.querySelector('input[name="xORoSelector"]:checked').value;
    let gameFieldArray = [0,0,0,0,0,0,0,0,0];
    document.body.removeChild(gameInvitationDiv);
    let gameField = document.createElement('table');
    gameField.id = 'gameField';
    for (let y = 0 ; y < 3 ; y++){
        let raw = document.createElement('tr');
        raw.id = 'raw'+y;
        gameField.appendChild(raw);
        for (let x = 0 ; x < 3 ; x++){
            let cell = document.createElement('td');
            cell.id = 'cell'+x;
            raw.appendChild(cell);
                let tristate = document.createElement('span');
                tristate.class="tristate tristate-checkbox";
                tristate.type="radio";
                tristate.id="cell" + y + x + "-off";
                // tristate.name="item1"
                tristate.value="-1";
                tristate.checked;
            // <input type="radio" id="item1-state-null" name="item1" value="0">
            //     <input type="radio" id="item1-state-on" name="item1" value="1">
            //     <i></i>
            //     </span>
        }
    };
    document.body.appendChild(gameField);
    let restartGameDiv = document.createElement('div');
    restartGameDiv.id = 'restartGameDiv';
    let restartGameBtn = document.createElement('input');
    restartGameBtn.type = 'button';
    restartGameBtn.value = 'Let\'s restart';
    restartGameBtn.onclick = restartGame;
    restartGameDiv.appendChild(restartGameBtn);
    document.body.appendChild(restartGameDiv);
};

